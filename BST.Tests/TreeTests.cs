using System;
using Xunit;

namespace BST.Tests
{
    public class TreeTests
    {
        [Fact]
        public void First_Value_Inserted_Equals_Root_Node()
        {
            //Arrange
            var tree = new BinaryTree();

            //Act
            tree.Insert(10);

            var node = tree.Find(10);

            //Assert
            Assert.True(node.IsRoot);
        }

        [Fact]
        public void Value_Smaller_Than_Parent_Is_Left_Child()
        {
            //Arrange
            var tree = new BinaryTree();

            //Act
            tree.Insert(10);
            tree.Insert(5);

            var node = tree.Find(10);

            //Assert
            Assert.True(node.LeftChild.Value == 5);
        }

        [Fact]
        public void Value_Greater_Than_Parent_Is_Right_Child()
        {
            //Arrange
            var tree = new BinaryTree();

            //Act
            tree.Insert(10);
            tree.Insert(15);

            var node = tree.Find(10);

            //Assert
            Assert.True(node.RightChild.Value == 15);
        }

        [Fact]
        public void Deleting_The_Root_Node_With_Children_Should_Throw_An_InvalidOperationExpection()
        {
            //Arrange
            var tree = new BinaryTree();

            //Act
            tree.Insert(10);
            tree.Insert(5);

            //Assert
            Assert.Throws<InvalidOperationException>(() => tree.Remove(10));
        }

        [Fact]
        public void A_Node_Without_Children_Is_A_Leaf()
        {
            //Arrange
            var tree = new BinaryTree();

            //Act
            tree.Insert(10);

            var node = tree.Find(10);

            //Assert
            Assert.True(node.IsLeaf);
        }
    }
}

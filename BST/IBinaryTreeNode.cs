﻿namespace BST
{
    public interface IBinaryTreeNode
    {
        int Depth { get; }

        bool IsLeaf { get; }

        bool IsRoot { get; set; }

        IBinaryTreeNode LeftChild { get; set; }

        IBinaryTreeNode RightChild { get; set; }

        IBinaryTreeNode Parent { get; set; }

        int Value { get; }

        IBinaryTreeNode Insert(int key);

        void Remove();

        IBinaryTreeNode Find(int key);

        IBinaryTreeNode FindMin(IBinaryTreeNode node);

        IBinaryTreeNode FindMax(IBinaryTreeNode node);

        void SetParent(IBinaryTreeNode parent);
    }
}

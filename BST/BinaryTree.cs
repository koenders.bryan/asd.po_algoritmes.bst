﻿using System;

namespace BST
{
    public class BinaryTree
    {
        private BinaryTreeNode root;

        public void Insert(int key)
        {
            if(root == null)
            {
                root = new BinaryTreeNode
                {
                    Value = key,
                    IsRoot = true
                };
            }

            //check if key/value already exists
            var node = root.Find(key);

            //if exists return;
            if(node != null)
            {
                return;
            }

            root.Insert(key);
        }

        public void Remove(int key)
        {
            IBinaryTreeNode node = root.Find(key);

            //no node found, nothing to remove
            if (node == null)
            {
                return;
            }       
            
            //check if node is the root node and if it still has children, throw if it is the case
            if(node.IsRoot && !node.IsLeaf)
            {
                throw new InvalidOperationException("Cannot remove the root node when the node still has children");
            }

            node.Remove();
        }

        public IBinaryTreeNode Find(int key)
        {
            return root.Find(key);
        }

        public IBinaryTreeNode FindMin()
        {
            return root.FindMin(root);
        }

        public IBinaryTreeNode FindMax()
        {
            return root.FindMax(root);
        }
    }
}
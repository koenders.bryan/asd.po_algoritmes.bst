﻿using System;

namespace BST
{
    public class Program
    {
        static void Main(string[] args)
        {
            BinaryTree binaryTree = new BinaryTree();

            binaryTree.Insert(43);
            binaryTree.Insert(3);
            binaryTree.Insert(13);
            binaryTree.Insert(22);
            binaryTree.Insert(9);
            binaryTree.Insert(17);
            binaryTree.Insert(6);

            Console.ReadLine();
        }
    }
}

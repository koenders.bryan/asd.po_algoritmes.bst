﻿namespace BST
{
    public class BinaryTreeNode : IBinaryTreeNode
    {
        public int Depth { get; }

        public int NodeSize { get; }

        public bool IsRoot { get; set; }

        public bool IsLeaf => LeftChild == null && RightChild == null;

        public bool IsOnLeftSide => Value < Parent.Value;

        public bool IsOnRightSide => Value >= Parent.Value;

        public IBinaryTreeNode LeftChild { get; set; }

        public IBinaryTreeNode RightChild { get; set; }

        public IBinaryTreeNode Parent { get; set; }

        public int Value { get; set;  }

        public BinaryTreeNode()
        {
            Depth = Parent != null ? Parent.Depth + 1 : 0;
        }

        public IBinaryTreeNode Insert(int value)
        {
            // value smaller than parent's value, insert left otherwise insert right
            if (value < Value)
            {
                if (LeftChild == null)
                {
                    LeftChild = new BinaryTreeNode
                    {
                        Value = value,
                        Parent = this
                    };

                    return LeftChild;
                }
                else
                {
                    LeftChild.Insert(value);
                }                    
            }
            else
            {
                if (RightChild == null)
                {
                    RightChild = new BinaryTreeNode
                    {
                        Value = value,
                        Parent = this
                    };

                    return RightChild;
                }
                else
                {
                    RightChild.Insert(value);
                }                    
            }

            return null;
        }


        public void Remove()
        {
            if (IsOnLeftSide)
            {
                Parent.LeftChild = null;
            }                
            else
            {
                Parent.RightChild = null;
            }                

            //current node does not have any children
            if (IsLeaf)
            {
                return;
            }

            //has left child but no right child, set parent of child to the current node's parent
            if (RightChild == null && LeftChild != null)
            {
                LeftChild.SetParent(Parent);  
            }                
            //has right child but no left child, set parent of child to the current node's parent
            else if (RightChild != null && LeftChild == null)
            {
                RightChild.SetParent(Parent); 
            }       
            //rotate current node's parent to the right child and set the left child's parent to the right child
            else
            {
                LeftChild.SetParent(RightChild);
                RightChild.SetParent(Parent);
            }
        }

        public void SetParent(IBinaryTreeNode parent)
        {
            //If the value is smaller than the parent value, this should be the left child.
            if (Value < parent.Value)
            {
                if (parent.LeftChild == null)
                {
                    parent.LeftChild = this;
                }
                else
                {
                    // The parent element already has a left child, we cannot override so we delegate this through 
                    //using a recursing function, placing the node UNDER the parent it's left child.
                    SetParent(parent.LeftChild);
                    return;
                }
            }
            else
            {
                if (parent.RightChild == null)
                {
                    parent.RightChild = this;
                }
                else
                {
                    SetParent(parent.RightChild);
                    return;
                }
            }

            Parent = parent;
        }

        public IBinaryTreeNode Find(int value)
        {
            //value found
            if (value == Value)
            {
                return this;
            }                

            if (value < Value)
            {
                if (LeftChild == null)
                {
                    return null;
                }                    
                else
                {
                    return LeftChild.Find(value);
                }
            }
            else
            {
                if (RightChild == null)
                {
                    return null;
                }                    
                else
                {
                    return RightChild.Find(value);
                }
                    
            }
        }

        public IBinaryTreeNode FindMin(IBinaryTreeNode node)
        {
            return node.LeftChild == null 
                ? node 
                : FindMin(node.LeftChild);
        }

        public IBinaryTreeNode FindMax(IBinaryTreeNode node)
        {
            return node.RightChild == null 
                ? node 
                : FindMax(node.RightChild);
        }
    }
}